#image: alpine:3.15.1
include:
  # no need to set the full url
  ## https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml
  template: Jobs/SAST.gitlab-ci.yml
  ## Gitlab detects your code's language automatically

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH != "main" && $CI_PIPELINE_SOURCE != "merge_request_event"      
      when: never
    - when: always

variables:
  IMAGE_NAME: $CI_REGISTRY_IMAGE
  #IMAGE_TAG: "1.1"
  DEV_ENDPOINT: "http://ec2-3-74-42-10.eu-central-1.compute.amazonaws.com:3000"
  DEV_SRV_HOST: "172.31.37.30"
  DEV_APP_PORT: "3000"
  STG_ENDPOINT: "http://ec2-3-74-42-10.eu-central-1.compute.amazonaws.com:4000"
  STG_SRV_HOST: "172.31.37.30"
  STG_APP_PORT: "4000"
  PRD_ENDPOINT: "http://ec2-3-74-42-10.eu-central-1.compute.amazonaws.com:5000"
  PRD_SRV_HOST: "172.31.37.30"
  PRD_APP_PORT: "5000"

stages:
  - test
  - build
  - deploy_dev
  - deploy_stg
  - deploy_prd

run_unit_test:
  stage: test
  image: node:18.6.0-alpine3.15
  tags:
    - ec2
    - docker
    - remote
  cache:
    # 2 different purposes: 1. Generate the cache 2. Download the cache
    key: "$CI_COMMIT_REF_NAME"
    # __paths__ is used to choose which files or directories to cache
    paths:
      - app/node_modules
  before_script:
    - cd app/
    - npm install
  script:
    - npm test
  artifacts:
    when: always
    paths:
      - "app/junit.xml"
    reports:
      junit: app/junit.xml

sast:
  stage: test
  
build_image:
  stage: build
  tags:
    - ec2
    - shell
    - remote
  before_script:
    - export PACKAGE_JSON_VERSION=$(cat app/package.json | jq -r .version)
    - export VERSION=$PACKAGE_JSON_VERSION.$CI_PIPELINE_IID
    - echo $VERSION > version-file.txt
    - echo "VERSION=$VERSION" >> build.env
    - echo "MY_ENV=superlab" >> build.env
  script:
    - docker build -t $IMAGE_NAME:$VERSION .
  artifacts:
    reports:
      dotenv: build.env
    paths:
      - version-file.txt

push_image:
  stage: build
  needs:
    - build_image
  ### __needs__ tells gitlab push_image needs to wait with exectution until build_image completed
  ### __dependencies__ tells gitlab push_image needs artifacts from build_image job
  dependencies:
    - build_image
  tags:
    - ec2
    - shell
    - remote
  before_script:
    - if [ -z ${CI_REGISTRY_USER+x} ]; then echo "CI_REGISTRY_USER is unset"; else echo "CI_REGISTRY_USER is set to '$CI_REGISTRY_USER'"; fi
    - export VERSION=$(cat version-file.txt)
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com
  script:
    - docker push $IMAGE_NAME:$VERSION


## job starting with a dot are jobs that should be extended.
.deploy:
  tags: 
    - ec2
    - shell
    - remote
  dependencies:
    - build_image
  variables:
    SSH_KEY: ""
    SRV_HOST: ""
    DEPLOY_ENV: ""
    APP_PORT: ""
    ENDPOINT: ""
  before_script:
    - echo $SSH_KEY | sed -e "s/-----BEGIN RSA PRIVATE KEY-----/&\n/" -e "s/-----END RSA PRIVATE KEY-----/\n&/" -e "s/\S\{64\}/&\n/g"\ > deploy-key.pem
    - chmod 400 deploy-key.pem
    - export VERSION=$(cat version-file.txt)
  script:
    - scp -o StrictHostKeyChecking=no -i deploy-key.pem ./docker-compose.yaml ubuntu@$SRV_HOST:/home/ubuntu
    - ssh -o StrictHostKeyChecking=no -i deploy-key.pem ubuntu@$SRV_HOST "
        docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY &&

        export COMPOSE_PROJECT_NAME=$DEPLOY_ENV
        export DC_IMAGE_NAME=$IMAGE_NAME && 
        export DC_IMAGE_TAG=$VERSION &&
        export DC_APP_PORT=$APP_PORT

        docker-compose down &&
        docker-compose up -d"
  environment:
    name: $DEPLOY_ENV
    url: $ENDPOINT


deploy_to_dev:
  extends: .deploy
  stage: deploy_dev
  needs: 
    - build_image
  variables:
    SSH_KEY: $SSH_PRIVATE_KEY
    SRV_HOST: $DEV_SRV_HOST
    DEPLOY_ENV: development
    APP_PORT: $DEV_APP_PORT
    ENDPOINT: $DEV_ENDPOINT

run_functionnal_test_dev:
  stage: deploy_dev
  needs:
    - deploy_to_dev
  script:
    - echo "running functionnal tests"

deploy_to_staging:
  extends: .deploy
  stage: deploy_stg
  needs: 
    - build_image
    - run_functionnal_test_dev
  variables:
    SSH_KEY: $SSH_PRIVATE_KEY
    SRV_HOST: $STG_SRV_HOST
    DEPLOY_ENV: staging
    APP_PORT: $STG_APP_PORT
    ENDPOINT: $STG_ENDPOINT


run_performance_test_stg:
  stage: deploy_stg
  needs:
    - deploy_to_staging
  script: 
    - echo "Running performance tests"

deploy_to_prod:
  extends: .deploy
  stage: deploy_prd
  needs: 
    - build_image
    - run_performance_test_stg
  variables:
    SSH_KEY: $SSH_PRIVATE_KEY
    SRV_HOST: $PRD_SRV_HOST
    DEPLOY_ENV: production
    APP_PORT: $PRD_APP_PORT
    ENDPOINT: $PRD_ENDPOINT
  when: manual

include:
  - template: Jobs/SAST.gitlab-ci.yml
